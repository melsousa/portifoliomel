<?php
session_start();
require_once("Controle/SadinheiroC.class.php");
echo"
<!DOCTYPE HTML>
<html lang = 'PT-BR'>
<head>
<meta charset='UTF-8'>
<title>Serviços</title>
    <link rel='stylesheet' type='text/css' href='css/bootstrap.min.css'>
    <link rel='stylesheet' type='text/css' href='css/fontawesome-free/css/all.min.css'>
    <link rel='stylesheet' type='text/css' href='css/simple-line-icons/css/simple-line-icons.css'>
    <link rel='stylesheet' type='text/css' href='css/landing-page.min.css'>
    <link rel='stylesheet' type='text/css' href='css/cssteste.css'>
    <link rel='stylesheet' type='text/css' href='css/main_styles.css'>
    <link rel='stylesheet' type='text/css' href='css/responsive.css'>
</head>
<body>
    <nav class= 'navbar navbar-expand-lg navbar-dark bg-dark fixed-top'>
        <div class='container'>
        <a class='navbar-brand' href='#'>Banco BMY</a>  
        </div>
    </nav>
    <br><br><br>
<center><div class='card mb-3' style='max-width: 900px;'>
    <div class='row no-gutters'>
      <div class='col-md-4'>
       <img src='img/card4.jpg' class='card-img' alt='...'>
      </div>
      <div class='col-md-8'>
      <div class='card-body'>       
      <table class='table table-striped'>
  <thead>
    <tr>
      <th scope='col'>#</th>
      <th scope='col'>Serviços para administrar o seu Cartão.</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope='row'>1</th>
      <td><a class='nav-link' href='#seuscartoes'>Seus Cartões</a></td>
     
    </tr>
    <tr>
      <th scope='row'>2</th>
      <td><a class='nav-link' href='#seusinvestimentos'>Seus investimentos</a></td>
      
    </tr>
    <tr>
      <th scope='row'>3</th>
      <td><a class='nav-link' href='#seussaques'>Seus saques</a></td>     
    </tr>

  </tbody>
</table>
</div>
</div>
</div>
</div>
</div></center>
<h2 id= 'seuscartoes'>Seus Cartões</h2>
    <div class= 'container'>
    <div class='row'>
      <div class='col-sm-9 col-md-7 col-lg-5 mx-auto'>
        <div class='card card-signin my-5'>
          <div class='card-body'>
            <h5 class='card-title text-center'>Adicionar Cartão</h5>
                <form class='form-signin' action='credito.php' method='post'>
                <div class='form-label-group'>
                <input type='text' name='nomeCartao' class='form-control' placeholder='Nome do Portador' required >
                <label for='nomeCartao'>Nome do cartao</label>
                </div>

                <div class='form-label-group'>
                <input type='number' name='credito' class='form-control' placeholder='Nº do cartão' required autofocus>
                <label for='credito'>Limite do cartão</label>
                </div>

  
                <hr class='my-4'>                
                <button class='btn btn-lg btn-primary btn-block text-uppercase' type='submit'>Adicionar</button>    
            </form>
          </div>
        </div>
      </div>
    </div>
    </div>

<h2  id='seusinvestimentos'>Seus Investimentos</h2>
<div class= 'container'>
    <div class='row'>
      <div class='col-sm-9 col-md-7 col-lg-5 mx-auto'>
        <div class='card card-signin my-5'>
          <div class='card-body'>
            <h5 class='card-title text-center'>Adicionar Investimento</h5>
            <form class='form-signin' action='enDinheiro.php' method='post'>
               <div class='form-label-group'>
                <input type='number' name='salario' class='form-control' placeholder='salario' required autofocus>
                <label for='salario'>Valor do salário</label>
               </div>
               <div class='form-label-group'>
                <input type='number' name='poupanca' class='form-control' placeholder='poupanca' required >
                <label for='poupanca'>Valor da poupança</label>
               </div>  
               <div class='form-label-group'>
               <input type='number' name='heranca' class='form-control' placeholder='heranca' required >
               <label for='heranca'>Valor da herança</label>
              </div> 
       
               <hr class='my-4'>                
               <button class='btn btn-lg btn-primary btn-block text-uppercase' type='submit'>Adicionar Investimento</button>    
            </form>
          </div>
        </div>
      </div>
    </div>
</div>

<h2  id ='seussaques' >Seus Saques</h2>
<div class= 'container'>
    <div class='row'>
      <div class='col-sm-9 col-md-7 col-lg-5 mx-auto'>
        <div class='card card-signin my-5'>
          <div class='card-body'>
            <h5 class='card-title text-center'>Efetuar Saques</h5>
                <form class='form-signin' action='saDinheiro.php' method='post'>
                <div class='form-label-group'>
                <input type='number' name='conta' class='form-control' placeholder='conta' required>
                <label for='conta'>Valor saque</label>
                </div>
                <div class='form-label-group'>
                <input type='text' name='legenda' class='form-control' placeholder='descricao' required>
                <label for='legenda'>Descrição do gasto</label>
                </div>
           
                <hr class='my-4'>                
                <button class='btn btn-lg btn-primary btn-block text-uppercase' type='submit'>Sacar</button>    
            </form>
            <br>
            <a href='Salario.php' class='btn btn-primary btn-lg btn-block' role='button' aria-pressed='true'>Sacar de Sálario</a>
            <br>
            <a href='poupanca.php' class='btn btn-primary btn-lg btn-block' role='button' aria-pressed='true'>Sacar de Poupança</a>
            <br>
            <a href='heranca.php' class='btn btn-primary btn-lg btn-block' role='button' aria-pressed='true'>Sacar de Herança</a>
          </div>
        </div>
      </div>
    </div>
    </div>
";
echo"<div class='card' style='width: 18rem;'>
<table border='1' class='table table-bordered table-dark'>
  
<tr>
    <th>Conta</th>
    <th>Descrição</td>
</tr>";

    $controle= new SadinheiroC();
    $controleM=$controle->selecionarId($_SESSION['cpf']);
    if($controleM!=null){
      foreach($controleM as $mostrar){
        echo" 
          <tr>
              <td>{$mostrar->getConta()}</td>
              <td>{$mostrar->getDescricao()}</td>
          </tr>
          

        ";
      }
    } 
    
echo"
</table></div>

<footer class='footer bg-dark'>
<div class='container'>
  <div class='row'>
    <div class='col-lg-6 h-100 text-center text-lg-left my-auto'>
      <ul class='list-inline mb-2'>
        <li class='list-inline-item'>
          <a href='#'>Sobre</a>
        </li>
        <li class='list-inline-item'>&sdot;</li>
        <li class='list-inline-item'>
          <a href='#'>Contato</a>
        </li>
        <li class='list-inline-item'>&sdot;</li>
        <li class='list-inline-item'>
          <a href='#'>Termos e Uso</a>
        </li>
        <li class='list-inline-item'>&sdot;</li>
        <li class='list-inline-item'>
          <a href='#'>Política de Privacidade</a>
        </li>
      </ul>
      <p class='text-muted small mb-4 mb-lg-0'>&copy; Criado em 2019 por alunas da Escola Estadual de Educação Paulo Petrola</p>
    </div>
    <div class='col-lg-6 h-100 text-center text-lg-right my-auto'>
      <ul class='list-inline mb-0'>
        <li class='list-inline-item mr-3'>
          <a href='#'>
            <i class='fab fa-facebook fa-2x fa-fw'></i>
          </a>
        </li>
        <li class='list-inline-item mr-3'>
          <a href='#'>
            <i class='fab fa-twitter-square fa-2x fa-fw'></i>
          </a>
        </li>
        <li class='list-inline-item'>
          <a href='#'>
            <i class='fab fa-instagram fa-2x fa-fw'></i>
          </a>
        </li>
      </ul>
    </div>
  </div>
</div>
</footer>


  <script src='js/jquery-3.2.1.min.js'></script>
  <script src='js/bootstrap.min.js'></script>
  <script src='js/TweenMax.min.js'></script>
  <script src='js/ScrollMagic.min.js'></script>
  <script src='js/animation.gsap.min.js'></script>
  <script src='js/custom.js'></script>
  <script src='js/jquery.js'></script>
  <script src='js/popper.min.js'></script>
  <script src='js/bootstrap.min.js'></script>
  <script src='js/bootstrap.bundle.min.js'></script>
  <script src='vendor/jquery/jquery.min.js'></script>
  <script src='vendor/bootstrap/js/bootstrap.bundle.min.js'></script>

</body>
</html>
";
?>