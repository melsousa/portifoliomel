<?php
class Credito{
	private $id;
	private $cpf;
	private $limitec;
	private $nomecartao;
	public function getId(){
		return $this->id;
	}
	public function setId($id){
		$this->id = ($id != NULL) ? $id : NULL;
	}
	public function getcpf(){
		return $this->cpf;
	}
	public function setcpf($cpf){
		$this->cpf = ($cpf != NULL) ? $cpf : NULL;
	}
	public function getLimitec(){
		return $this->limitec;
	}
	public function setLimitec($limitec){
		$this->limitec = ($limitec != NULL) ? $limitec : NULL;
	}
	public function getNomecartao(){
		return $this->nomecartao;
	}
	public function setNomecartao($nomecartao){
		$this->nomecartao = ($nomecartao != NULL) ? $nomecartao : NULL;
	}
}
?>