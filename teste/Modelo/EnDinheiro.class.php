<?php
 	class EnDinheiro{
 		private $id;
 		private $salario;
 		private $financeiro;
 		private $poupanca;
		private $heranca; 
		private $cpf;

 		public function getId(){
 			return $this->id;
 		}
 		public function setId($id){
 			$this->id = ($id != NULL) ? $id : NULL;
 		}
 		public function getSalario(){
 			return $this->salario;
 		}
 		public function setSalario($salario){
 			$this->salario = ($salario != NULL) ? $salario : NULL;
 		}
 		public function getFinanceiro(){
 			return $this->financeiro;
 		}
 		public function setFinanceiro($financeiro){
 			$this->financeiro = ($financeiro != NULL) ? $financeiro : NULL;
 		}
 		public function getPoupanca(){
 			return $this->poupanca;
 		}
 		public function setPoupanca($poupanca){
 			$this->poupanca = ($poupanca != NULL) ? $poupanca : NULL;
 		}
 		public function getHeranca(){
 			return $this->heranca;
 		}
 		public function setHeranca($heranca){
 			$this->heranca = ($heranca != NULL) ? $heranca : NULL;
		 }
		 public function getCpf(){
			return $this->cpf;
		}
		public function setCpf($cpf){
			$this->cpf = ($cpf != NULL) ? $cpf : NULL;
		}
 	}
?>