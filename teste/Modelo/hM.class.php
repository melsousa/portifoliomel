<?php
    class hM{
        private $id_hist;
        private $id_usuario;
        private $id_credito;
        private $nome;
        private $data;
        private $valorE;
        private $valorS;

        public function getIdHist(){
            return $this->id_hist;
        }
        public function setIdHist($id_hist){
            $this->id_hist = $id_hist;
        }

        public function getIdCredito(){
            return $this->id_credito;
        }
        public function setIdCredito($credito){
            $this->id_credito = $credito;
        }

        public function getIdUsuario(){
            return $this->id_usuario;
        }
        public function setIdUsuario($usuario){
            $this->id_usuario = $usuario;
        }

        public function getNomeRegis(){
            return $this->nome;
        }
        public function setNomeRegis($m){
            $this->nome = $m;
        }
        public function getData(){
            return $this->data;
        }
        public function setData($d){
            $this->data = $d;
        }
        public function getValorE(){
            return $this->valorE;
        }
        public function setValorE($p){
            $this->valorE = $p;
        }        
        public function getValorS(){
            return $this->valorS;
        }
        public function setValorS($a){
            $this->valorS = $a;
        }
    }
?>