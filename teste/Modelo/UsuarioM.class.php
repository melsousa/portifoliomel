<?php
class UsuarioM{
	private $cpf;
	private $nome;
	private $senha;
	public function getCpf(){
		return $this->cpf;
	}
	public function setCpf($cpf){
		$this->cpf = ($cpf != NULL) ? $cpf : NULL;
	}
	public function getNome(){
		return $this->nome;
	}
	public function setNome($nome){
		$this->nome = ($nome != NULL) ? $nome : NULL;
	}
	public function getSenha(){
		return $this->senha;
	}
	public function setSenha($senha){
		$this->senha = ($senha != NULL) ? $senha : NULL;
	}
}
?>