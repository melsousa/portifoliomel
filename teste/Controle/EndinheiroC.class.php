<?php
require_once("Conexao.class.php");
require_once("Modelo/EnDinheiro.class.php");
final class EndinheiroC{ 
    /*Método de seleção do ID*/  
    public function selecionarId($id){
            $conexao = new Conexao("Controle/xuxu.ini");
            $comando = $conexao->getConexao()->prepare("SELECT * FROM enDinheiro WHERE cpf=:cpf");
            $comando->bindParam("cpf", $id);
            $comando->execute();
            /*fetch: um array ?*/
            $consulta = $comando->fetch();
            $modelo = new EnDinheiro();
            $modelo->setId($consulta->id);
            $modelo->setSalario(floatval($consulta->salario));
            $modelo->setPoupanca(floatval($consulta->poupanca));
            $modelo->setHeranca(floatval($consulta->heranca));
            $modelo->setCpf($consulta->cpf);
            $conexao->__destruct();
            return $modelo;   
        }
        /*Método de seleção de todos*/
        public function selecionarTodos(){
            $conexao = new Conexao("Controle/xuxu.ini");
            $comando = $conexao->getConexao()->prepare("SELECT * FROM enDinheiro WHERE cpf=:cpf");
            $comando->execute();
            /*fetcAll:*/
            $resultado = $comando->fetchAll();
            $lista = [];
            foreach($resultado as $item){
                $atualizar = new EnDinheiro();
                $atualizar->setId($item->id); 
                $atualizar->setSalario($item->salario);
                $atualizar->setPoupanca($item->poupanca);
                $atualizar->setHeranca($item->heranca);
                $atualizar->setCpf($item->cpf);
                array_push($lista, $atualizar);
            }
            $conexao->__destruct();
            return $lista;
        }
        
        /*Método de adição*/
        public function adicionar($novo){
                $conexao = new Conexao("Controle/xuxu.ini");
                $sql= "INSERT INTO enDinheiro(salario,poupanca,heranca,cpf) VALUES(:s,:p,:h,:c)";
                $comando = $conexao->getConexao()->prepare($sql);
                $comando->bindValue("s", $novo->getSalario());
                $comando->bindValue("p", $novo->getPoupanca());
                $comando->bindValue("h", $novo->getHeranca());
                $comando->bindValue("c", $novo->getCpf());
                if($comando->execute()){
                    $conexao->__destruct();
                    return true;
                    }else{
                    $conexao->__destruct();
                }	

        }
        /*Método de deletar*/
        public function deletar($salario){
            $conexao = new Conexao("Controle/xuxu.ini");
            $comando = $conexao->getConexao()->prepare("DELETE FROM enDinheiro WHERE id = :id AND salario = :salario");
            $comando->bindValue("salario", $salario);
        }
        /*Método de atualização */
        public function atualizar($atualizar){
            $conexao= new Conexao("Controle/xuxu.ini");
            $sql = "UPDATE enDinheiro SET salario=:sa,poupanca=:pou,heranca=:her WHERE cpf=:cpf;";
            $comando= $conexao->getConexao()->prepare($sql);
            $comando->bindValue("sa", $atualizar->getSalario());
            $comando->bindValue("pou", $atualizar->getPoupanca());
            $comando->bindValue("her", $atualizar->getHeranca());
            $comando->bindValue("cpf", $atualizar->getCpf());
            $comando->execute();
            $conexao->__destruct();
            return true;
        }        
    }
?>