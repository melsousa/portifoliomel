<?php
require_once("Conexao.class.php");
require_once("Modelo/Credito.class.php");
    final class CreditoC{
        public function selecionarId($id){
            $conexao = new Conexao("Controle/xuxu.ini");
			$comando = $conexao->getConexao()->prepare("SELECT * FROM credito WHERE cpf=:cpf");
			$comando->bindParam("id", $id);
			$comando->execute();
			$consulta = $comando->fetch();
			$adicionar = new Credito();
            $adicionar->setId($consulta->id);      
            $adicionar->setCpf($consulta->cpf);
            $adicionar->setLimitec(floatval($consulta->limitec));
            $adicionar->setNomecartao($consulta->nomecartao);
            $conexao->__destruct();
            return $adicionar;
        }
        public function adicionar($novo){
            $conexao = new Conexao("Controle/xuxu.ini");
                $sql= "INSERT INTO credito(cpf,limitec,nomecartao) VALUES(:c,:l,:n)";
                $comando = $conexao->getConexao()->prepare($sql);
                $comando->bindValue("c", $novo->getCpf());
                $comando->bindValue("l", $novo->getLimitec()); 
                $comando->bindValue("n", $novo->getNomecartao());
                if($comando->execute()){
            		$conexao->__destruct();
            		return true;
       	 		}else{
            		$conexao->__destruct();
        		}		
				
        }
    }
?>