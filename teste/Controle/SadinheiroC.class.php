<?php
require_once("Conexao.class.php");
require_once("Modelo/SaDinheiro.class.php");
    final class SaDinheiroC{
        public function adicionar($novo){
            $conexao = new Conexao("Controle/xuxu.ini");
            $sql= "INSERT INTO saDinheiro(conta,descricao,cpf) VALUES(:o,:d,:c)";
            $comando = $conexao->getConexao()->prepare($sql);
            $comando->bindValue("o", $novo->getConta());
            $comando->bindValue("d", $novo->getDescricao());
            $comando->bindValue("c", $novo->getCpf());
            if($comando->execute()){
                $conexao->__destruct();
                return true;
                }else{
                $conexao->__destruct();
            }	

        }
        public function selecionarId($id){
            $conexao = new Conexao("Controle/xuxu.ini");
            $comando = $conexao->getConexao()->prepare("SELECT * FROM saDinheiro WHERE cpf=:cpf");
            $comando->bindParam("cpf", $id);
            $lista=[];
            $comando->execute();
            $consulta = $comando->fetchAll();
            foreach($consulta as $consultaL){
                $modelo = new SaDinheiro();
                $modelo->setId($consultaL->id);
                $modelo->setCpf($consultaL->cpf);
                $modelo->setConta(floatval($consultaL->conta));
                $modelo->setDescricao($consultaL->descricao);
                array_push($lista, $modelo);
            }
           $conexao->__destruct();
           return $lista;             
        }
    }
    
?>