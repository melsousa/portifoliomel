create database banco;
 use banco;
 create table usuario(cpf varchar(60) primary key not null, nome varchar(60) not null, senha varchar(60) not null);
 create table enDinheiro(id int primary key not null auto_increment,salario float,financeiro float,poupanca float,heranca float,cpf varchar(60), FOREIGN KEY(cpf) REFERENCES usuario(cpf));
 create table conta(id int primary key not null auto_increment,limpagamento date,cefetuadas float);
 create table credito(id int primary key not null auto_increment,cpf varchar(60), FOREIGN KEY(cpf) REFERENCES usuario(cpf), limitec float,nomecartao varchar(60));
 create table saDinheiro(id int primary key not null auto_increment,conta float,descricao varchar(100),cpf varchar(60), FOREIGN KEY(cpf) REFERENCES usuario(cpf));	
create table historico(id_hist int primary key auto_increment not null, id_usuario varchar(200), id_credito varchar(200), data varchar(20) not null, nome varchar(55) not null, valore double, valors double);