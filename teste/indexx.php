<?php
session_start();
echo"
<!DOCTYPE HTML>
<html lang = 'PT-BR'>

<head>

  <meta charset='utf-8'>
  <title>BMY</title>

  <link rel='stylesheet' type='text/css' href='css/bootstrap.min.css'>
  <link rel='stylesheet' type='text/css' href='css/fontawesome-free/css/all.min.css'>
  <link rel='stylesheet' type='text/css' href='css/simple-line-icons/css/simple-line-icons.css'>
  <link rel='stylesheet' type='text/css' href='css/landing-page.min.css'>
  <link rel='stylesheet' type='text/css' href='css/cssteste.css' >
  <link rel='stylesheet' type='text/css' href='css/main_styles.css'>
  <link rel='stylesheet' type='text/css' href='css/responsive.css'>

</head>

<body>


<nav class= 'navbar navbar-expand-lg navbar-dark bg-dark fixed-top'>
<div class='container'>
  <a class='navbar-brand' href='#'>Banco BMY</a>
  <button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarResponsive' aria-controls='navbarResponsive' aria-expanded='false' aria-label='Toggle navigation'>
        <span class='navbar-toggler-icon'></span>
      </button>
  <div class='collapse navbar-collapse' id='navbarResponsive'>
    <ul class='navbar-nav ml-auto'>
      <li class='nav-item active'>
        <a class='nav-link' href='#'>Página inicial
              <span class='sr-only'>(current)</span>
            </a>
      </li>
      <li class='nav-item'>
        <a class='nav-link' href='#sobrenos'>Sobre nós</a>
      </li>
      ";
      if (isset($_SESSION['user'])) {
        echo"
      <li class='nav-item'>
        <a class='nav-link' href='servicos.php'>Serviços</a>
      </li>
      <li class='nav-item'>
      <a class='nav-link' href='sair.php'>Sair</a>
    </li>
    ";
      }else{
        echo"
        <li class='nav-item'>
        <a class='nav-link' href='cadastro.php'>Cadastre-se</a>
      </li>
        <li class='nav-item'>
        <a class='nav-link' href='login.php'>Login</a>
      </li>
    ";
    }
    echo"
    </ul>
  </div>
</div>
</nav>

<div class= 'bd-example'>
<div id='carouselExampleCaptions' class='carousel slide' data-ride='carousel'>
  <ol class='carousel-indicators'>
    <li data-target='#carouselExampleCaptions' data-slide-to='0' class='active'></li>
    <li data-target='#carouselExampleCaptions' data-slide-to='1'></li>
    <li data-target='#carouselExampleCaptions' data-slide-to='2'></li>
  </ol>
  <div class='carousel-inner'>
    <div class='carousel-item active'>
      <img src='img/card3.jpg' class='d-block w-100' alt='...'>
      <div class='carousel-caption d-none d-md-block'>
        <h2 class='a'>Acesse agora onde vocé estiver</h2>
        <p><h4 class='b'>Agora você pode fazer seu monitoramento finaceiro no conforto de sua casa.</h4></p>
      </div>
    </div>
    <div class='carousel-item'>
      <img src='img/card2.jpg' class='d-block w-100' alt='...'>
      <div class='carousel-caption d-none d-md-block'>
        <h2 class='a'>Na palma da mão</h2>
        <p><h4 class='b'>Muito mais praticidade, acesse em qualquer hora e lugar.</h4></p>
      </div>
    </div>
    <div class='carousel-item'>
      <img src='img/card.jpg' class='d-block w-100' alt='...'>
      <div class='carousel-caption d-none d-md-block'>
        <h2 class='a'>Facilidade e Simplicidade</h2>
        <p><h4 class='b'>Um designer super simples acessível para todos os usuários. </h4></p>
      </div>
    </div>
  </div>
  <a class='carousel-control-prev' href='#carouselExampleCaptions' role='button' data-slide='prev'>
    <span class='carousel-control-prev-icon' aria-hidden='true'></span>
    <span class='sr-only'>Previous</span>
  </a>
  <a class='carousel-control-next' href='#carouselExampleCaptions' role='button' data-slide='next'>
    <span class='carousel-control-next-icon' aria-hidden='true'></span>
    <span class='sr-only'>Next</span>
  </a>
</div>
</div>

 
<div class='col magic_fade_in'>
  <section class='features-icons bg-light text-center'>
    <div class='container'>
      <div class='row'>
        <div class='col-lg-4'>
          <div class='features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3'>
            <div class='features-icons-icon d-flex'>
              <i class='icon-screen-desktop m-auto text-primary'></i>
            </div>
            <h3>Finanças</h3>
            <p class='lead mb-0'>Tenha controle dos seus cartões, contas a pagar e a receber, salário e mais! Tenha toda informação que você precisa disponível no computador ou celular.</p>
          </div>
        </div>
        <div class='col-lg-4'>
          <div class='features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3'>
            <div class='features-icons-icon d-flex'>
              <i class='icon-layers m-auto text-primary'></i>
            </div>
            <h3>Organização</h3>
            <p class='lead mb-0'>Pare de guardar recibos e notas nas gavetas e tenha uma visão geral de todas as suas transações na tela do celular ou do computador.</p>
          </div>
        </div>
        <div class='col-lg-4'>
          <div class='features-icons-item mx-auto mb-0 mb-lg-3'>
            <div class='features-icons-icon d-flex'>
              <i class='icon-check m-auto text-primary'></i>
            </div>
            <h3>Fácil de Usar</h3>
            <p class='lead mb-0'>Basta se cadastrar e pronto! Faça login e comece já a utilizar nossos serviços e comece seu controle financeiro.</p>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

  <div class='col magic_fade_in'>
  <section class='testimonials text-center bg-light'><span id='sobrenos' class='sobrenos>
    <div class='container'>
    <h2 class='mb-5'>Conheça nossas desenvolvedoras</h2><br>
      <div class='row'>
        <div class='col-lg-4'>
          <div class='testimonial-item mx-auto mb-5 mb-lg-0'>
            <img class='img-fluid rounded-circle mb-3' src='img/Bea.jpg'>
            <h5>Ana Byatriz</h5>
            <p class='font-weight-light mb-0'>Alguma descrição que a Bea vai fazer. Sla, fala sobre o que tu sabe e gosta de fazer e bla bla bla</p>
          </div>
        </div>
        <div class='col-lg-4'>
          <div class='testimonial-item mx-auto mb-5 mb-lg-0'>
            <img class='img-fluid rounded-circle mb-3' src='img/Mel.jpg'>
            <h5>Melissa Sousa</h5>
            <p class='font-weight-light mb-0'>Alguma descrição que a Mel vai fazer. Sla, fala sobre o que tu sabe e gosta de fazer e bla bla bla</p>
          </div>
        </div>
        <div class='col-lg-4'>
          <div class='testimonial-item mx-auto mb-5 mb-lg-0'>
            <img class='img-fluid rounded-circle mb-3' src='img/Mim.jpg'>
            <h5>Yasmim Rocha</h5>
            <p class='font-weight-light mb-0'>Preciso de vocês pra ter inspiração k k</p>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

  <footer class='footer bg-dark'>
    <div class='container'>
      <div class='row'>
        <div class='col-lg-6 h-100 text-center text-lg-left my-auto'>
          <ul class='list-inline mb-2'>
            <li class='list-inline-item'>
              <a href='#'>Sobre</a>
            </li>
            <li class='list-inline-item'>&sdot;</li>
            <li class='list-inline-item'>
              <a href='#'>Contato</a>
            </li>
            <li class='list-inline-item'>&sdot;</li>
            <li class='list-inline-item'>
              <a href='#'>Termos e Uso</a>
            </li>
            <li class='list-inline-item'>&sdot;</li>
            <li class='list-inline-item'>
              <a href='#'>Política de Privacidade</a>
            </li>
          </ul>
          <p class='text-muted small mb-4 mb-lg-0'>&copy; Criado em 2019 por alunas da Escola Estadual de Educação Paulo Petrola</p>
        </div>
        <div class='col-lg-6 h-100 text-center text-lg-right my-auto'>
          <ul class='list-inline mb-0'>
            <li class='list-inline-item mr-3'>
              <a href='#'>
                <i class='fab fa-facebook fa-2x fa-fw'></i>
              </a>
            </li>
            <li class='list-inline-item mr-3'>
              <a href='#'>
                <i class='fab fa-twitter-square fa-2x fa-fw'></i>
              </a>
            </li>
            <li class='list-inline-item'>
              <a href='#'>
                <i class='fab fa-instagram fa-2x fa-fw'></i>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </footer>

  <script src='js/jquery-3.2.1.min.js'></script>
  <script src='js/bootstrap.min.js'></script>
  <script src='js/TweenMax.min.js'></script>
  <script src='js/ScrollMagic.min.js'></script>
  <script src='js/animation.gsap.min.js'></script>
  <script src='js/custom.js'></script>
  <script src='js/jquery.js'></script>
  <script src='js/popper.min.js'></script>
  <script src='js/bootstrap.min.js'></script>
  <script src='js/bootstrap.bundle.min.js'></script>
  <script src='vendor/jquery/jquery.min.js'></script>
  <script src='vendor/bootstrap/js/bootstrap.bundle.min.js'></script>

</body>

</html>
";
?>