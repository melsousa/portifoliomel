<?php
echo"
<!DOCTYPE HTML>
<html lang = 'PT-BR'>

<head>

<meta charset='UTF-8'>
<title>Login</title>

    <link rel='stylesheet' href='css/bootstrap.min.css'>
    <link rel='stylesheet' href='css/cssteste.css' >

</head>

<body>

<nav class= 'navbar navbar-expand-lg navbar-dark bg-dark fixed-top'>
<div class='container'>
  <a class='navbar-brand' href='indexx.php'>Banco BMY</a>
</div>
</nav>
<br></br>

<div class= 'container'>
<div class='row'>
  <div class='col-sm-9 col-md-7 col-lg-5 mx-auto'>
    <div class='card card-signin my-5'>
      <div class='card-body'>
        <h5 class='card-title text-center'>Login</h5>
        <form class='form-signin' action='log.php' method='post'>
          <div class='form-label-group'>
           <input type='text' name='cpf' class='form-control' placeholder='cpf' required autofocus>
           <label for='cpf'>CPF</label>
         </div> 

          <div class='form-label-group'>
            <input type='text' name='nome' class='form-control' placeholder='nome' required autofocus>
            <label for='nome'>Usuário</label>
          </div>

          <div class='form-label-group'>
            <input type='password' name='senha' class='form-control' placeholder='senha' required>
            <label for='senha'>Senha</label>
          </div>
          <hr class='my-4'>
          
          <button class='btn btn-lg btn-primary btn-block text-uppercase' type='submit'>Entrar</button>

        </form>
      </div>
    </div>
  </div>
</div>
</div>



<script src='js/jquery-3.2.1.min.js'></script>
<script src='js/jquery.min.js'></script>
<script src='js/bootstrap.bundle.min.js'></script>

</body>
 
  ";
  ?>